const offre = require('../controllers/OffreController')
const express = require('express')

//ROUTER
exports.router = (function () {
    const apiRoutesoffre = express.Router()
    apiRoutesoffre.route('/offre/add').post(offre.createOffre)
    apiRoutesoffre.route('/offre/get').get(offre.getOffre)
    apiRoutesoffre.route('/offre/update/:id').put(offre.updateOffre)
    return apiRoutesoffre;
})();