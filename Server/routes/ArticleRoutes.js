const article = require('../controllers/ArticleController')
const express = require('express')

//ROUTER
exports.router = (function () {
    const apiRoutesArticle = express.Router()
    apiRoutesArticle.route('/article/add').post(article.createProduct)
    apiRoutesArticle.route('/article/get').get(article.getProducts)
    apiRoutesArticle.route('/article/update/:id').put(article.updateProduct)
    apiRoutesArticle.route('/article/delete/:id').delete(article.deleteProduct)
    return apiRoutesArticle;
})();