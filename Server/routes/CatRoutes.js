const categorie = require('../controllers/CatController')
const express = require('express')

//ROUTER
exports.router = (function () {
    const apiRoutes = express.Router()

    apiRoutes.route('/categorie/add').post(categorie.createCategory)
    apiRoutes.route('/categorie/get').get(categorie.getCategories)
    apiRoutes.route('/categorie/update/:id').put(categorie.updateCategory)
    apiRoutes.route('/categorie/delete/:id').delete(categorie.deleteCategory)


    return apiRoutes;
})();