const router = require('express').Router();
const cloudinary = require('cloudinary');
const fs = require('fs');

// Configuration de Cloudinary
cloudinary.v2.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.CLOUD_API_KEY,
    api_secret: process.env.CLOUD_API_SECRET_KEY
});

// Route pour télécharger une image
router.post('/upload', (req, res) => {
    try {
        if (!req.files || Object.keys(req.files).length === 0)
            return res.status(400).json({ msg: 'Aucun fichier n\'a été téléchargé' });

        const file = req.files.file;

        // Vérification de la taille du fichier
        if (
            (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') && // Vérification pour les images
            file.size > 50 * 1024 * 1024
        ) {
            removeTemp(file.tempFilePath);
            return res.status(400).json({ msg: 'La taille de l\'image est trop grande' }); 
        } else if (
            (file.mimetype === 'video/mp4') && // Vérification pour les vidéos MP4
            file.size > 100 * 1024 * 1024 // Exemple de taille maximale (50 Mo)
        ) {
            removeTemp(file.tempFilePath);
            return res.status(400).json({ msg: 'La taille de la vidéo est trop grande' });
        }

        // Vérification du type de fichier
        if (
            file.mimetype !== 'image/jpeg' &&
            file.mimetype !== 'image/png' &&
            file.mimetype !== 'application/pdf' &&
            file.mimetype !== 'video/mp4' &&
            file.mimetype !== 'audio/mpeg'
        ) {
            removeTemp(file.tempFilePath);
            return res.status(400).json({ msg: 'Le format du fichier est incorrect' });
        }

        // Upload du fichier sur Cloudinary
        cloudinary.v2.uploader.upload_large(file.tempFilePath, { folder: 'ProjectEcom' }, async (err, result) => {
            if (err) {
                removeTemp(file.tempFilePath);
                return res.status(500).json({ msg: 'Erreur lors de l\'upload sur Cloudinary' });
            }
            removeTemp(file.tempFilePath);
            res.json({ public_id: result.public_id, url: result.secure_url });
        });
    } catch (err) {
        return res.status(500).json({ msg: err.message });
    }
});



// Route pour supprimer une image
router.post('/destroy', (req, res) => {
    try {
        const { public_id } = req.body;
        if (!public_id) return res.status(400).json({ msg: 'Aucune image sélectionnée' });

        cloudinary.v2.uploader.destroy(public_id, async (err, result) => {
            if (err) throw err;
            res.json({ msg: 'Image supprimée avec succès' });
        });
    } catch (err) {
        return res.status(500).json({ msg: err.message });
    }
});

// Fonction pour supprimer un fichier temporaire
const removeTemp = (path) => {
    fs.unlink(path, err => {
        if (err) throw err;
    });
};

module.exports = router;