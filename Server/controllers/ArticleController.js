const Products = require('../models/Article');


//sort, filtering, paginating

class APIfeatures {
    constructor(query,queryString){
        this.query=query;
        this.queryString= queryString;
    }
    filtering(){
        const queryObj = {...this.queryString}

        const excludedFields = ['page','sort','limit']
        excludedFields.forEach(el => delete(queryObj[el]))


        let queryStr = JSON.stringify(queryObj)
        queryStr = queryStr.replace(/\b(gte|gt|lt|lte|regex)\b/g, match => '$' + match)

        this.query.find(JSON.parse(queryStr))

        return this;
    }
    sorting(){
        if(this.queryString.sort){
            const sortBy= this.queryString.sort.split(',').join(' ')

            this.query = this.query.sort(sortBy)

        }else{
            this.query = this.query.sort('createdAt')
        }
        return this;
    }
    paginating(){
        const page = this.queryString.page * 1 || 1
        const limit = this.queryString.limit * 1 || 9
        const skip = (page - 1 ) * limit;
        this.query = this.query.skip(skip).limit(limit)
        return this;
    }
}

    const articleCtrl = {
        getProducts: async (req,res) =>{
            try {
                const features = new APIfeatures(Products.find(), req.query).filtering().sorting().paginating()
                const products = await features.query

                return res.json({
                    status: "success",
                    result: products.length,
                    products
                })
            } catch (err) {
                return res.status(500).json({msg : err.message})
            }
        },
        createProduct: async (req,res) =>{
            try {
                const {reference, nom, caracteristiques, img, video, catalogue, categorie} = req.body;
               // if(!img) return res.status(400).json({msg:'No image upload'})

                const product = await Products.findOne({reference})

                if(product) return res.status(400).json({msg: 'This product already exist'})

                const newProduct = new Products({
                    reference , nom, caracteristiques, img, video, catalogue, categorie
                })
                await newProduct.save()
                return res.json({msg:'Created the Product'})
            } catch (err) {
                return res.status(500).json({msg : err.message})
            }
        },
        deleteProduct: async (req,res) =>{
            try {
                await Products.findByIdAndDelete(req.params.id)
                res.json({msg:'Deleted the Product'})
            } catch (err) {
                return res.status(500).json({msg : err.message})
            }
        },
        updateProduct: async (req,res) =>{
            try {
                const { reference, nom, caracteristiques, img, video, catalogue, categorie} = req.body;
                if(!img) return res.status(400).json({msg:'No image upload'})
                await Products.findOneAndUpdate({_id: req.params.id},{
                    reference, nom, caracteristiques, img, video, catalogue, categorie
                })
                return res.json({msg:"Updated the Product"})
            } catch (err) {
                return res.status(500).json({msg : err.message})
            }
        },


    }
    module.exports = articleCtrl