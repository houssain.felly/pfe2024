const Offre = require('../models/Offre');


//sort, filtering, paginating

class APIfeatures {
    constructor(query,queryString){
        this.query=query;
        this.queryString= queryString;
    }
    filtering(){
        const queryObj = {...this.queryString}

        const excludedFields = ['page','sort','limit']
        excludedFields.forEach(el => delete(queryObj[el]))


        let queryStr = JSON.stringify(queryObj)
        queryStr = queryStr.replace(/\b(gte|gt|lt|lte|regex)\b/g, match => '$' + match)

        this.query.find(JSON.parse(queryStr))

        return this;
    }
    sorting(){
        if(this.queryString.sort){
            const sortBy= this.queryString.sort.split(',').join(' ')

            this.query = this.query.sort(sortBy)

        }else{
            this.query = this.query.sort('createdOf')
        }
        return this;
    }
    paginating(){
        const page = this.queryString.page * 1 || 1
        const limit = this.queryString.limit * 1 || 9
        const skip = (page - 1 ) * limit;
        this.query = this.query.skip(skip).limit(limit)
        return this;
    }
}

const offreCtrl = {
    getOffre: async (req,res) =>{
        try {
            const features = new APIfeatures(Offre.find(), req.query).filtering().sorting().paginating()
            const offre = await features.query

            return res.json({
                status: "success",
                result: offre.length,
                offre
            })
        } catch (err) {
            return res.status(500).json({msg : err.message})
        }
    },
    createOffre: async (req,res) =>{
        try {
            const {ref, nom, Tonnage, caract} = req.body;

            const offre = await Offre.findOne({ref})
            if(offre) return res.status(400).json({msg: 'This offre already exist'})

            const newOffre = new Offre({
                ref , nom, Tonnage, caract
            })
            await newOffre.save()
            return res.json({msg:'Created the Offre'})
        } catch (err) {
            return res.status(500).json({msg : err.message})
        }
    },
    updateOffre: async (req,res) =>{
        try {
            const { ref, nom, Tonnage, caract} = req.body;
            await Offre.findOneAndUpdate({_id: req.params.id},{
                ref, nom, Tonnage, caract
            })
            return res.json({msg:"Updated the Offre"})
        } catch (err) {
            return res.status(500).json({msg : err.message})
        }
    },


}
module.exports = offreCtrl