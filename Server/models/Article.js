const mongoose = require('mongoose');

const articleSchema = new mongoose.Schema({
    reference: {
        type: String,
        unique: true,
        trim: true,
        required: true
    },
    nom: {
        type: String,
        trim: true,
        required: true
    },
    caracteristiques: {
        type: String,
        trim: true,
        required: true
    },
    img: {
        type: Object,

        required: true
    },
    video: {
        type: Object,
        required: true
    },
    catalogue: {
        type: Object,
        required: true
    },
    categorie: {
        type: String,
        trim: true,
        required: true
    }
});

const Article = mongoose.model('Article', articleSchema);

module.exports = Article;