const mongoose = require('mongoose');

const offreSchema = new mongoose.Schema({
    ref: {
        type:String,
        unique:true,
        trim:true,
        required:true
    },
    nom: {
        type:String,
        trim:true,
        required:true
    },
    Tonnage: {
        type:String,
        trim:true,
        required:true
    },
    caract:{
        type:String,

        required:true
    }
});

module.exports  = mongoose.model('Offre', offreSchema);