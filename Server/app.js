require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');
// const hpp = require('hpp');
// const cors = require('cors');
const AppError = require('./utils/appError');
const globalErrorHandler = require('./controllers/errorController');

const userRouter = require('./routes/userRoutes');

const cors = require('cors');

const app = express();

const fileUpload= require('express-fileupload');

app.use(cors())
app.use(fileUpload({
    useTempFiles:true
}));



app.use(express.json());

// 1) GLOBAL MIDDLEWARES

// CORS middleware
const allowedOrigins = ['http://localhost:5173'];
/*const corsOptions = {
  origin: function(origin, callback) {
    if (allowedOrigins.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
  credentials: true,
  optionsSuccessStatus: 200
};
*/

// Définir les CORS pour permettre l'accès depuis le front-end
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*'); // Autoriser l'accès depuis n'importe quel domaine (à adapter en fonction de vos besoins)
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});


// Configure CORS
 // Ajoutez ici l'URL de votre application cliente
 const corsOptions = {
  origin: function(origin, callback) {
    if (allowedOrigins.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
  credentials: true
};


app.use(cors(corsOptions));

// Set security HTTP headers
//app.use(helmet());

// Development logging
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

// Limit requests from same API
// const limiter = rateLimit({
//   max: 100,
//   windowMs: 60 * 60 * 1000,
//   message: 'Too many requests from this IP, please try again in an hour!'
// });
// app.use('/api', limiter);

// Body parser, reading data from body into req.body
app.use(express.json());

// Data sanitization against NoSQL query injection
app.use(mongoSanitize());

// Data sanitization against XSS
app.use(xss());

// Serving static files
app.use(express.static(`${__dirname}/public`));

// 3) ROUTES
app.use('/api/users', userRouter);
// app.use('/api/machine', machineRouter);

// Définition des routes API
const apiRoutesArticle = require('../Server/routes/ArticleRoutes').router;
app.use('/api/', apiRoutesArticle);

const apiRoutes = require('../Server/routes/CatRoutes').router;
app.use('/api/', apiRoutes);
app.use("/api/",require('../Server/Uploads'));



// Offre Routes
const apiRoutesoffre = require('../Server/routes/OffreRoutes').router;
app.use('/api/', apiRoutesoffre);




// HANDLING UNHANDLED ROUTES
app.all('*', (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server!`, 404));
});

// GLOBAL ERROR MIDDLEWARE
app.use(globalErrorHandler);

module.exports = app;
