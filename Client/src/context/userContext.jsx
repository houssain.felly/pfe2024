import axios from "axios"
import { createContext , useState , useEffect } from "react"

export const UserContext = createContext({})


export function UserContextProvider({children}) {

    const [user , setUser] = useState(null)

    useEffect(() => {
        if(!user) {
            axios.get("/users/me").then(({data}) => {
                setUser(data)
                console.log("USER IS HERE FROM THE COOKIES" , data.data.data)
            })

        }

    })

    return(
        <UserContext.Provider value={{user , setUser}} >
            {children}


        </UserContext.Provider>
    )



}