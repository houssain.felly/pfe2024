import { useEffect, useState } from "react";
import axios from "axios";
import toast from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import { useAuthContext } from "./useAuthContext";

export const useLogin = () => {
  const navigate = useNavigate();
  const { dispatch } = useAuthContext();
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const handleUserRedirect = (userRole) => {
    console.log("🎯 UERROLE :" , userRole)
    if (userRole === "client") navigate("/");
    else if (userRole === "admin") navigate("/admindashboard");
  };

  useEffect(() => {
    return () => {};
  }, []);

  const login = async (email, password) => {
    setIsLoading(true);
    setError(null);

    axios
      .post("/users/signin", { email, password })
      .then((response) => {
        const json = response.data;
        if (response.status === 200) {
          localStorage.setItem("user", JSON.stringify(json));
          dispatch({ type: "LOGIN", payload: json });
          handleUserRedirect(json.user.role);

        } else {
          setError(json.error);
        }
      })
      .catch((error) => {
        if (error.response) {
          toast.error(error.response.data.message);
        } else {
          setError("An error occurred during login");
        }
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return { login, isLoading, error };
};
