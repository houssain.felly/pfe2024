import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router'



const Dashboard = () => {
  const [suc, setSuc] = useState()
  const navigate = useNavigate()
  axios.defaults.withCredentials = true;
  useEffect(() => {
    axios.get('http://localhost:3001/dash')
      .then(res => {
        if (res.data === "Success") {
          setSuc("Success OK")
        } else {
          navigate('/Log')
        }
      }).catch(err => console.log(err))
  }, [])
  return (
    <div>
      <h2>Dashboard Page</h2>
      <p>{suc}</p>
    </div>
  )
}

export default Dashboard
