import React from 'react';
import { Link } from 'react-router-dom';
import './article.css';

const Article = () => {
  return (
    <div className="container">
      <Link to="/AddArt" className="link">Ajouter</Link>
      <Link to="/UpdateArt" className="link Update">Modifier</Link>
      <Link to="/DropArt" className="link delete">Supprimer</Link>
    </div>
  );
};

export default Article;
