import React, { useState } from 'react';
import './article.css'

const AddArticle = () => {
    const [formData, setFormData] = useState({
        reference: '',
        nom: '',
        caracteristiques: '',
        img: null,
        video: null,
        catalogue: null,
        categorie: '',
      });
    
      const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({
          ...formData,
          [name]: value,
        });
      };
    
      const handleImageUpload = (e) => {
        const file = e.target.files[0];
        setFormData({
          ...formData,
          img: file,
        });
      };
    
      const handleVideoUpload = (e) => {
        const file = e.target.files[0];
        setFormData({
          ...formData,
          video: file,
        });
      };
    
      const handleCatalogueUpload = (e) => {
        const file = e.target.files[0];
        setFormData({
          ...formData,
          catalogue: file,
        });
      };
    
      const handleSubmit = (e) => {
        e.preventDefault();
      };
    
      return (
        <form onSubmit={handleSubmit}>
          <div className='colonne1'>
            <div className="form-group">
              <label htmlFor="textInput1">Reference:</label>
              <input
                type="text"
                name="textInput1"
                value={formData.textInput1}
                onChange={handleChange}
                className="input-text"
              />
            </div>
            <div className="form-group">
              <label htmlFor="textInput2">Nom:</label>
              <input
                type="text"
                name="textInput2"
                value={formData.textInput2}
                onChange={handleChange}
                className="input-text"
              />
            </div>
            <div className="form-group">
              <label htmlFor="textInput3">Caracteristique:</label>
              <input
                type="text"
                name="textInput3"
                value={formData.textInput3}
                onChange={handleChange}
                className="input-text"
              />
            </div>
          </div>
          <div className='colonne2'>
            <div className="form-group">
              <label htmlFor="imageFile">Image:</label>
              <input
                type="file"
                id="imageFile"
                accept="image/*"
                onChange={handleImageUpload}
                className="input-file"
              />
              <label htmlFor="imageFile" className="input-file-label">
                Choose Image
              </label>
            </div>
            <div className="form-group">
              <label htmlFor="videoFile">Video:</label>
              <input
                type="file"
                id="videoFile"
                accept="video/*"
                onChange={handleVideoUpload}
                className="input-file"
              />
              <label htmlFor="videoFile" className="input-file-label">
                Choose Video
              </label>
            </div>
            <div className="form-group">
              <label htmlFor="catalogueFile">Catalogue:</label>
              <input
                type="file"
                id="catalogueFile"
                accept=".pdf,.doc,.docx"
                onChange={handleCatalogueUpload}
                className="input-file"
              />
              <label htmlFor="catalogueFile" className="input-file-label">
                Choose Catalogue
              </label>
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="categorie">Catégorie:</label>
            <select
              id="categorie"
              name="categorie"
              value={formData.categorie}
              onChange={handleChange}
              className="input-select">
    
              <option value="">Sélectionner une catégorie</option>
              <option value="Hydrolique">Hydrolique</option>
              <option value="Electrique">Electrique</option>
              <option value="Robots">Robots</option>
              <option value="Péripherique">Péripherique</option>
            </select>
          </div>
    
          <button type="submit" className="submit-btn">Submit</button>
        </form>
      );
    };

export default AddArticle
