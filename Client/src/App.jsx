import { BrowserRouter } from "react-router-dom";

import { AuthContextProvider } from "./context/AuthContext";
import routes, { renderRoutes } from "./routes.jsx";

import { Toaster } from "react-hot-toast";


const App = () => {
  return (
    <AuthContextProvider>
       <Toaster
          position="top-center"
          reverseOrder={true}
          toastOptions={{ duration: 2000 }}
        />
      <BrowserRouter>{renderRoutes(routes)}</BrowserRouter>
    </AuthContextProvider>
  );
};

export default App;
