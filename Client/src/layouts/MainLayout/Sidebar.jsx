import { useEffect, useState } from "react";
import { useAuthContext } from "../../hooks/useAuthContext";


import { DiGoogleAnalytics } from "react-icons/di";
import { TbCarCrane } from "react-icons/tb";
import { BsPersonFillGear } from "react-icons/bs";
import { MdCarRepair } from "react-icons/md";
import { FaBoxesPacking } from "react-icons/fa6";
import { TbLogout2 } from "react-icons/tb";
import { PiNoteFill } from "react-icons/pi";
import { LinkOutlined} from "@ant-design/icons";
import { Menu } from "antd";

import { useLogout } from "../../hooks/useLogout";


import LOGO from "../../assets/hero/logo.png";
import LOGO1 from "../../assets/hero/logo1.png";
import { Link, useNavigate } from "react-router-dom";

function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}


const Sidebar = () => {
  const { logout } = useLogout();
  const handleLogOut = () => {
    logout();
  };
  const mode = "inline";
  const navigate = useNavigate();

  const [toggleSidebar, setToggleSidebar] = useState(false);
  const changeToggleSidebar = () => {
    setToggleSidebar(!toggleSidebar);
    console.log("Sidebar", toggleSidebar);
  };
  const items = [
    getItem(
      "MENU RAPIDE",
      "menuRapide",
      null,
      [
        getItem(
          "Tableau de Board",
          "/admindashboard",
          <DiGoogleAnalytics />,
          null
        )
      ],
      "group"
    ),

    getItem(
      "GESTION",
      "gestion",
      null,
      [
        getItem("Clients", "/clients", <BsPersonFillGear />, null),
        getItem("Offre", "/tab-offre", <FaBoxesPacking />, null),
        getItem("Commande", "/commande", <MdCarRepair />, null),
        getItem("Categorie", "/categorie", <TbCarCrane />, null),
        getItem("Article", "/article", <MdCarRepair />, null),
        getItem("Transporteurs", "/transporteurs", <TbCarCrane />, null),
      ],
      "group"
    ),

  ];
  const itemsMiniSidebarItems = [
    { icon: <BsPersonFillGear />, link: "/clients" },
    { icon: <FaBoxesPacking />, link: "/tab-offre"},
    { icon: <MdCarRepair />, link: "/commande"},
    { icon: <TbCarCrane />, link: "/categorie" },
    { icon: <MdCarRepair />, link: "/article"},
    { icon: <TbCarCrane />, link: "/transporteurs" },
   
  ];

  return (
    <>
      {!toggleSidebar ? (
        <div className="sidebar">
          <div className="logo-wrapper">
            <div className="logo" onClick={changeToggleSidebar}>
              <img src={LOGO} alt="logo" />
            </div>
            <div className="title">P M I</div>
          </div>

          <div className="menu-wrapper">
            <Menu
              onClick={({ key }) => {
                navigate(key);
              }}
              className="sidebar-menu"
              defaultSelectedKeys={["1"]}
              mode={mode}
              items={items}
            />
            <div className="menu-btns-wrapper">
              <a href="https://haitianinter.com/en/" target="_blank"  rel="noreferrer">
                <div className="website-link-btn">
                  <div className="website-link-icon">
                    <LinkOutlined />
                  </div>
                  <div className="website-link-title">Haitian Siteweb</div>
                </div>
              </a>
              <Link to="/" onClick={handleLogOut}>
                <div className="logout-btn">
                  <div className="logout-icon">
                    <TbLogout2 />
                  </div>
                  <div className="logout-title">Se déconnecter</div>
                </div>
              </Link>
            </div>
          </div>
        </div>
      ) : (
        <div className="mini-sidebar">
          <div className="mini-sidebar-logo-wrapper">
          <div className="logo" onClick={changeToggleSidebar}>
              <img src={LOGO1} alt="logo" />
            </div>
          </div>

          <div className="mini-sidebar-menu">
            <ul>
              {itemsMiniSidebarItems.map((item, index) => (
                <Link to={item.link} key={index}>
                  <li className="mini-sidebar-item">
                    <div className="mini-sidebar-item-icon">{item.icon}</div>
                  </li>
                </Link>
              ))}
            </ul>
          </div>
        </div>
      )}
    </>
  );
};

export default Sidebar;