import { useLogout } from "../../hooks/useLogout";
import { useAuthContext } from "../../hooks/useAuthContext";
import { useEffect, useState } from "react";

import { IconButton } from "@mui/material";

import { FaCaretDown } from "react-icons/fa";
import { TbLogout2 } from "react-icons/tb";
import { LuUserSquare2 } from "react-icons/lu";


import AVATAR from "../../assets/images/Profile_avatar.png";

import { Input } from "antd";

const Header = () => {
  const { logout } = useLogout();
  const { user } = useAuthContext();
  const [showDropdown, setShowDropdown] = useState(false);
  const [userRole, setUserRole] = useState("");
  const [userName, setUserName] = useState("");

  useEffect(() => {
    if (user) {
      const role = user.user.role;
      if (
        [
          "admin",
        ].includes(role)
      ) {
        setUserRole("Administrator");
      } else {
        setUserRole(role ? role[0].toUpperCase() + role.slice(1) : "");
      }
      setUserName("@" + user.user.username);
    }
  }, [user]);

  const toggleDropdown = () => {
    setShowDropdown(!showDropdown);
  };

  const handleLogOut = () => {
    logout();
  };

  const { Search } = Input;

  const onSearch = (value, _e, info) => console.log(info?.source, value);

  return (
    <div className="header">
      <div className="header-search">
        <Search placeholder="Recherche" onSearch={onSearch} enterButton />
      </div>

      <div className="header-widgets">
        {/* <div className="widgets">
         
          <div className="widget">
            <MdDarkMode />
          </div>
          <div className="widget">
            <MdNotificationsActive />
            <div className="badge">5</div>
          </div>
        </div> */}

        <div className="profile-section">
          <div className="avatar">
            <img src={AVATAR} alt="" />
          </div>
          <div className="user">
            <div className="name">Maher Felly</div>
            <div className="role">Admin</div>
          </div>

          <IconButton className="drop-down-chevron" onClick={toggleDropdown}>
            <FaCaretDown className="chevron-down" />
          </IconButton>

           {showDropdown && (
            <div className="header-drop-down-menu-wrapper">
              <div className="header-drop-down-menu-item">
                <div className="icon-wrapper">
                  <LuUserSquare2 />
                </div>
                <div className="item-text">Profile</div>
              </div>

              <div
                className="header-drop-down-menu-item"
                onClick={handleLogOut}
              >
                <div className="icon-wrapper">
                  <TbLogout2 />
                </div>
                <div className="item-text">deconnecter</div>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
export default Header;