import Header from "./Header";
import Sidebar from "./Sidebar";
import "./index.scss"

const MainLayout = ({ children }) => {
  
  return (
    <>
      <div className="main-container">
        <div className="main_layout">
          <Sidebar />

          <div className="full_page_container">
            <Header />

            <div className="full_page_wrapper">{children}</div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MainLayout;

