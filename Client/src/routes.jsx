import { Suspense, Fragment, lazy } from "react";
import { Routes, Route } from "react-router-dom";
import MainLayout from "./layouts/MainLayout/index.jsx";
import LoadingScreen from "./components/LoadingScreen/LoadingScreen.jsx";

export const renderRoutes = (routes = []) => {
  // const isAuthenticated = localStorage.getItem("user") !== null;
  // let userRole = "";
  // if (isAuthenticated) {
  //     userRole = JSON.parse(localStorage.getItem("user")).user?.role;
  //     console.log("GETTING USER ROLE .... ROUTES");

  // }

  return (
    <Suspense fallback={<LoadingScreen />}>
      <Routes>
        {routes.map((route, i) => {
          const Layout = route.layout || Fragment;
          const Component = route.component;
          // const isProtected = route.protected || false;
          // const isAuthorized = route.roles ? route.roles.includes(userRole) : true;
          // console.log("REDIRECTING USER .... ROUTES");

          return (
            <Route
              key={i}
              path={route.path}
              element={
                <Layout>
                  {/* {isProtected && !isAuthenticated ? (
                      <Navigate to="/login" />
                    ) : (
                      isAuthorized ? <Component /> : <Navigate to="/unauthorized" />
                    )} */}

                  <Component />
                </Layout>
              }
            />
          );
        })}
      </Routes>
    </Suspense>
  );
};



const routes = [

  {
    path: "/",
    component: lazy(() => import("./views/Home/Home.jsx")),
  },
  {
    path: "/signin",
    component: lazy(() => import("./views/Authentication/Auth.jsx")),
  },
  {
    path: "/signup",
    component: lazy(() => import("./views/Authentication/Auth.jsx")),
  },
  {
    path: "/forgetPassword",
    component: lazy(() => import("./views/ForgetPassword/ForgetPassword.jsx")),
  },
  {
    path: "/offre",
    component: lazy(() => import("./components/Website/Offre/Offre.jsx")),
  },

  {
    path: "/UserArt",
    component: lazy(() => import("./components/products/Products.jsx")),
  },



  {
    path: "/admindashboard",
    layout: MainLayout,
    component: lazy(() => import("./views/Admin/AdminDashboard/AdminDashboard.jsx")),
    // protected: true,
    // roles: ['super-admin']
  },

  {
    path: "/clients",
    layout: MainLayout,
    component: lazy(() => import("./views/Admin/AdminDashboard/Client.jsx")),
    // protected: true,
    // roles: ['super-admin']
  },

  {
    path: "/AddArt",
    layout: MainLayout,
    component: lazy(() => import("./pages/Products/createProducts/CreateProduct.jsx")),
    // protected: true,
    // roles: ['super-admin']
  },

  {
    path: "/UpdateArt",
    layout: MainLayout,
    component: lazy(() => import("./pages/Article/UpdateArticle.jsx")),
    // protected: true,
    // roles: ['super-admin']
  },

  {
    path: "/DropArt",
    layout: MainLayout,
    component: lazy(() => import("./pages/Article/DropArticle.jsx")),
    // protected: true,
    // roles: ['super-admin']
  },

  {
    path: "/tab-offre",
    layout: MainLayout,
    component: lazy(() => import("./views/Admin/AdminDashboard/Offre.jsx")),
    // protected: true,
    // roles: ['super-admin']
  },

  {
    path: "/commande",
    layout: MainLayout,
    component: lazy(() => import("./views/Admin/AdminDashboard/Commande.jsx")),
    // protected: true,
    // roles: ['super-admin']
  },

  {
    path: "/transporteurs",
    layout: MainLayout,
    component: lazy(() => import("./views/Admin/AdminDashboard/Transporteur.jsx")),
    // protected: true,
    // roles: ['super-admin']
  },

  {
    path: "/categorie",
    layout: MainLayout,
    component: lazy(() => import("./pages/categorie/Categories.jsx")),
    // protected: true,
    // roles: ['super-admin']
  },
  
  {
    path: "/article",
    layout: MainLayout,
    component: lazy(() => import("./components/products/Products.jsx")),
    // protected: true,
    // roles: ['super-admin']
  },

  {
    path: "*",
    component: lazy(() => import("./views/NotFound/NotFound.jsx")),
  },

  {
    path: "/test",
    component: lazy(() => import("./components/LoadingScreen/LoadingScreen.jsx")),
  },

];

export default routes;
