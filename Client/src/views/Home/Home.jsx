import Navbar from '../../components/Website/Navbar/Navbar'
import Hero from '../../components/Website/Hero/Hero'
import Category from '../../components/Website/Category/Category'
import Description from '../../components/Website/Description/Description'
import Footer from '../../components/Website/Footer/Footer'
import Map from '../../components/Website/Map/Map'


const Home = () => {
  return (
    <div className='home-wrapper'>

      <div className="sticky top-0 left-0 right-0 z-40">
      <Navbar />
      </div>

      <Hero />
      <Description />
      <Category />
      <Map />
      <Footer />

    </div>
  )
}

export default Home
