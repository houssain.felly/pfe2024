import "./loginPage.scss";

import { Link } from "react-router-dom";
import { useEffect, useState } from "react";

import MINILOGO from "../../assets/Logos/miniLogo.png"

import { LuMails } from "react-icons/lu";
import { LuKeyRound } from "react-icons/lu";
import { IoMdLogIn } from "react-icons/io";


import { Input } from "antd";
import { Checkbox } from "antd";

import toast from "react-hot-toast";
import { useLogin } from "../../hooks/useLogin";

const LoginPage = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { login, error } = useLogin();

  const handleSubmit = async (e) => {
    // e.preventDefault();
    // await login(email, password);
    // if (error) {
    //   console.log("ERROR : ", error);
    //   toast.error(error);
    // }
  };

  

  return (
    <div className="full-page-container">
      <div className="login-page-container">
        <div className="login-page-left">
          <div className="mini-logo">
            <img src={MINILOGO} alt="main logo"/>
          </div>
          <div className="login-left-title">CONNECTEZ-VOUS !</div>
          <div className="login-left-form-wrapper">
            <div className="top">
              <div className="form-input-wrapper">
                <div className="icon-wrapper">
                  <LuMails />
                  <div className="mini-input-divider"></div>
                </div>
                <input
                  type="email"
                  placeholder="Email"
                  className="login-form-input"
                />
              </div>

              <div className="form-input-wrapper">
                <div className="icon-wrapper">
                  <LuKeyRound />
                  <div className="mini-input-divider"></div>
                </div>
                <input
                  type="password"
                  placeholder="Mot de passe"
                  className="login-form-input"
                />
              </div>
            </div>
            <div className="center">
            Mot de passe oublié ?
            </div>
            <div className="bottom">
              <div className="login-btn">
                <div className="icon-wrapper">
                  <IoMdLogIn/>
                </div>
                <div className="text">Se Connecter</div>
              </div>
            </div>
          </div>
        </div>
        <div className="login-page-right">
          {/* <div className="login-top">
            <img src={Polylogo} alt="" />
          </div>

          <div className="title">Welcome Back !</div>

          <div className="login-form-wrapper">
            <div className="form-item-wrapper">
              <label className="form-label">Email</label>
              <div className="form-input-wrapper">
                <div className="form-input-icon">
                  <BsPersonFillCheck />
                </div>

                <Input
                  placeholder=""
                  className="form-input"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
            </div>

            <div className="form-item-wrapper">
              <label className="form-label">Password</label>
              <div className="form-input-wrapper">
                <div className="form-input-icon">
                  <FaUnlockKeyhole className="icon-key"/>
                </div>

                <Input.Password
                  className="form-input"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  placeholder=""
                  
                />
              </div>
            </div>

            <button className="login-form-btn" onClick={handleSubmit}>
              Login
              <AiOutlineSwapRight className="arrow-icon-btn" />
            </button>
          </div> */}
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
