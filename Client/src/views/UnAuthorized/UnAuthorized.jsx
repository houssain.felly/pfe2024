import React from 'react'
import "./unAuthorized.scss"

import UnAuthorizedBG from "../../assets/images/protected.png"

const UnAuthorized = () => {
  return (
    <div className='unauthorized-page-wrapper'>
        <img src={UnAuthorizedBG} alt='unauthorized Page'/>


    </div>
  )
}

export default UnAuthorized