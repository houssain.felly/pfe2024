import { useState } from "react";
import "./authStyles.css";
import SignIn from "./SignIn/SignIn";
import SignUp from "./SignUp/SignUp";

export default function Auth() {
  // START LOGIC FOR CSS PURPOSES //
  const [type, setType] = useState("signIn");
  const handleOnClick = (text) => {
    if (text !== type) {
      setType(text);
      return;
    }
  };
  const containerClass =
    "test-container " + (type === "signUp" ? "right-panel-active" : "");
  // END LOGIC FOR CSS PURPOSES //

  return (
    <div>
     
      <div className="auth-wrapper">
        <div className={containerClass} id="container">
          <SignUp />
          <SignIn />
          <div className="overlay-container">
            <div className="overlay">
              <div className="overlay-panel overlay-left">
                <h1>Welcome Back!</h1>
                <p>
                  To keep connected with us please login with your personal info
                </p>
                <button
                  className="button-login ghost w-[100px] bg-brandContact text-white  mt-10 px-2 py-1 "
                  id="signIn"
                  onClick={() => handleOnClick("signIn")}
                >
                  Sign In
                </button>
              </div>
              <div className="overlay-panel overlay-right">
                <h1>Hello, Friend!</h1>
                <p>Enter your personal details and start journey with us</p>
                <button
                  className="ghost w-[100px] bg-brandContact text-white mt-10 px-2 py-1 "
                  id="signUp"
                  onClick={() => handleOnClick("signUp")}
                >
                  Sign Up
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
