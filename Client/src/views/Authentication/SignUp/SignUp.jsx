import { useState } from "react";
import "../authStyles.css";

import toast from "react-hot-toast";

import axios from "axios";

function SignUp() {
  const [userData, setUserData] = useState({
    email: "",
    username: "",
    company: "",
    phoneNumber: "",
    password: "",
    passwordConfirm: "",
    role: "client",
  });

  const handleSignUpRequest = async (e) => {
    e.preventDefault();
    console.log("🎯 SIGN UP REQUEST");

    e.preventDefault();

    const { email, username, company, phoneNumber, password, passwordConfirm, role } =
      userData;
    console.log(userData);

    try {
      const response = await axios.post("users/signup", {
        email,
        username,
        company,
        phoneNumber,
        password,
        passwordConfirm,
        role,
      });
      console.log("Response:", response.data);
      toast.success("user signup successfully !");

      setUserData({
        email: "",
        username: "",
        company: "",
        phoneNumber: "",
        password: "",
        passwordConfirm: "",
        role: "client",
      });
    } catch (error) {
      console.log("💥 ERROR :", error);
    }
  };

  return (
    <div className="form-containers sign-up-container">
      <form>
        <h1>Create Account</h1>

        <input
          type="text"
          name="username"
          placeholder="Username"
          autoComplete="off"
          className="formInput dark:bg-gray-900  dark:focus:border-white"
          value={userData.username}
          onChange={(e) =>
            setUserData({
              ...userData,
              username: e.target.value,
            })
          }
        />
        <input
          type="text"
          name="company"
          placeholder="company"
          autoComplete="off"
          className="formInput dark:bg-gray-900  dark:focus:border-white"
          value={userData.company}
          onChange={(e) =>
            setUserData({
              ...userData,
              company: e.target.value,
            })
          }
        />
        <input
          type="email"
          name="email"
          placeholder="Email"
          autoComplete="off"
          className="formInput dark:bg-gray-900  dark:focus:border-white"
          value={userData.email}
          onChange={(e) =>
            setUserData({
              ...userData,
              email: e.target.value,
            })
          }
        />
        <input
          type="text"
          name="phone"
          placeholder="Num Telephone"
          className="formInput dark:bg-gray-900  dark:focus:border-white"
          value={userData.phoneNumber}
          onChange={(e) =>
            setUserData({
              ...userData,
              phoneNumber: e.target.value,
            })
          }
        />
        <input
          type="password"
          name="password"
          placeholder="Password"
          className="formInput dark:bg-gray-900  dark:focus:border-white"
          value={userData.password}
          onChange={(e) =>
            setUserData({
              ...userData,
              password: e.target.value,
            })
          }
        />

        <input
          type="password"
          name="password"
          placeholder="Confirm Password"
          className="formInput dark:bg-gray-900  dark:focus:border-white"
          value={userData.passwordConfirm}
          onChange={(e) =>
            setUserData({
              ...userData,
              passwordConfirm: e.target.value,
            })
          }
        />

        <button
          className="w-[100px] rounded-xl
         bg-brandContact text-white active:bg-blue-600 focus:outline-none focus:ring focus:ring-violet-300 mt-10 px-2 py-1 "
          onClick={handleSignUpRequest}
        >
          Sign Up
        </button>
      </form>
    </div>
  );
}

export default SignUp;
