import "../authStyles.css";
import toast from "react-hot-toast";
import { useLogin } from "../../../hooks/useLogin";
import { useState } from "react";

function SignIn() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { login, error } = useLogin();

  const handleSignInRequest = async (e) => {
    e.preventDefault();
    console.log("🎯 SIGN IN REQUEST");

    await login(email, password);

    if (error) {
      console.log("ERROR : ", error);
      toast.error(error);
    }
  };

  return (
    <div className="form-containers sign-in-container">
      <form>
        <h1>Sign in</h1>

        <input
          type="email"
          placeholder="Email"
          name="email"
          className="formInput dark:bg-gray-900  dark:focus:border-white"
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <input
          type="password"
          placeholder="Password"
          name="password"
          required
          className="formInput dark:bg-gray-900  dark:focus:border-white"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <a href="#">Forgot your password?</a>
        <button
          className="w-[100px] rounded-xl
         bg-brandContact text-white active:bg-blue-600 focus:outline-none focus:ring focus:ring-violet-300 px-2 py-1 "
          onClick={handleSignInRequest}
        >
          Sign In
        </button>
      </form>
    </div>
  );
}

export default SignIn;
