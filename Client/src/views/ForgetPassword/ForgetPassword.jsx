import React from "react";
import "../ForgetPassword/forgetPassword.scss";
import logo1 from "../../assets/images/Logo1.png";
import ForgetPasswordBg from "../../assets/images/forgetPasswordBg.png";
import { MdOutlineSecurity } from "react-icons/md";

import { BsPersonFillCheck } from "react-icons/bs";
import { SiSpringsecurity } from "react-icons/si";
import { AiOutlineSwapRight } from "react-icons/ai";
import { Link } from "react-router-dom";

const ForgetPassword = () => {
  return (
    <div className="full-page-container">
      <div className="forgetPass-page-container">
        <div className="forgetPass-page-left">
          <img src={ForgetPasswordBg} alt="" />
          <div class="overlay">
            <div className="overlay-bottom">
              <div className="text">Back To Login?</div>
              <Link to="/login">
                <button>Click Here</button>
              </Link>
            </div>
          </div>
        </div>
        <div className="forgetPass-page-right">
          <div className="forgetPass-top">
            <img src={logo1} alt="" />
          </div>

          <div className="title">You Want To Reset Password ?</div>

          <div className="forgetPass-form-wrapper">
            <div className="form-item-wrapper">
              <label className="form-label"> Email </label>
              <div className="form-input-wrapper">
                <div className="form-input-icon">
                  <BsPersonFillCheck />
                </div>

                <input type="text" className="form-input" />
              </div>
            </div>

            <div className="form-item-wrapper">
              <label className="form-label"> Current Password</label>
              <div className="form-input-wrapper">
                <div className="form-input-icon">
                  <MdOutlineSecurity />
                </div>

                <input type="password" className="form-input" />
              </div>
            </div>

            <div className="form-item-wrapper">
              <label className="form-label"> New Password</label>
              <div className="form-input-wrapper">
                <div className="form-input-icon">
                  <SiSpringsecurity />
                </div>

                <input type="password" className="form-input" />
              </div>
            </div>

            <button className="forgetPass-form-btn">
              Login
              <AiOutlineSwapRight className="arrow-icon-btn" />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ForgetPassword;
