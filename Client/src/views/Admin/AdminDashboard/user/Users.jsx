import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function Users() {

    const trans = JSON.parse(localStorage.getItem("Trans"))?.user || null;

    const handleDelete = (id) => {
        axios.delete('http://localhost:5000/deleteUser/' + id)
            .then(res => {
                console.log(res)
                window.location.reload()
            })
            .catch(errr => console.log(errr))
    }

    return (
        <div className="d-flex vh-100 bg-primary justify-content-center align-items-center">
            <div className="w-50 bg-white rounded p-3">
                <Link to="/create" className='btn btn-success'>Add +</Link>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Num° Tel</th>
                            <th>Ville</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{trans.username}</td>
                            <td>{trans.phone}</td>
                            <td>{trans.Ville}</td>
                            <td>
                                <Link to={`/update/${trans._id}`} className='btn btn-success'>Update</Link>
                                <button className='btn btn-danger'
                                    onClick={(e) => handleDelete(trans._id)}>Delete</button>
                            </td>
                        </tr>
                    </tbody>
                </table>


            </div>

        </div>
    )
}
export default Users;