// Import necessary modules
import axios from 'axios';
import React, { useState, useEffect } from 'react';

function Transporteur() {
    // Define state variables
    const [transporteurList, setTransporteurList] = useState([]);
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [ville, setVille] = useState('');
    const [onEdit, setOnEdit] = useState(false);
    const [id, setId] = useState('');

    // Function to fetch transporteurs from backend
    const fetchTransporteur = () => {
        axios.get('/Transp/get')
            .then(response => {
                setTransporteurList(response.data);
            })
            .catch(error => {
                console.error('Error fetching transporteurs:', error);
            });
    };

    // Fetch transporteurs on component mount
    useEffect(() => {
        fetchTransporteur();
    }, []);

    // Function to handle form submission
    const createTransporteur = (e) => {
        e.preventDefault();
        if (onEdit) {
            axios.put(`/Transp/update/${id}`, { name, phone, ville })
                .then(() => {
                    alert("Transporteur updated successfully");
                    fetchTransporteur();
                })
                .catch(error => {
                    console.error('Error updating transporteur:', error);
                    alert("An error occurred while updating the transporteur");
                });
        } else {
            axios.post('/Transp/add', { name, phone, ville })
                .then(() => {
                    alert("Transporteur created successfully");
                    fetchTransporteur();
                })
                .catch(error => {
                    console.error('Error creating transporteur:', error);
                    alert("An error occurred while creating the transporteur");
                });
        }
        // Reset form fields and edit state
        setName('');
        setPhone('');
        setVille('');
        setOnEdit(false);
    };

    // Function to set form fields for editing
    const editTransporteurs = (id, name, phone, ville) => {
        setId(id);
        setName(name);
        setPhone(phone);
        setVille(ville);
        setOnEdit(true);
    };

    // Function to delete a transporter
    const deleteTransporteur = (id) => {
        axios.delete(`/Transp/delete/${id}`)
            .then(() => {
                alert("Transporteur deleted successfully");
                fetchTransporteur();
            })
            .catch(error => {
                console.error('Error deleting transporteur:', error);
                alert("An error occurred while deleting the transporteur");
            });
    };

    // Render component
    return (
        <div className="Transporteur">
            <form onSubmit={createTransporteur}>
                <label htmlFor="name">Name</label>
                <input type="text" name="name" value={name} required onChange={e => setName(e.target.value)} />
                <label htmlFor="phone">Phone Number</label>
                <input type="text" name="phone" value={phone} required onChange={e => setPhone(e.target.value)} />
                <label htmlFor="ville">City</label>
                <input type="text" name="ville" value={ville} required onChange={e => setVille(e.target.value)} />
                <button type="submit">{onEdit ? "Update" : "Save"}</button>
            </form>
            <div className="col">
                {transporteurList.map(transporteur => (
                    <div className="row" key={transporteur._id}>
                        <p>Name: {transporteur.name}</p>
                        <p>Phone: {transporteur.phone}</p>
                        <p>City: {transporteur.ville}</p>
                        <div>
                            <button className="edit" onClick={() => editTransporteurs(transporteur._id, transporteur.name, transporteur.phone, transporteur.ville)}>Edit</button>
                            <button onClick={() => deleteTransporteur(transporteur._id)}>Delete</button>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default Transporteur;
