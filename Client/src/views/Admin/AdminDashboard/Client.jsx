import React, { useEffect, useState } from "react";
import axios from "axios";
import './all.css';

function Client() {

  const user = JSON.parse(localStorage.getItem("user"))?.user || null;



  return (
    <div className="client-container">
      
      <table className="Table">
        <thead className="Tab-head">
          <tr>
            <th>Nom</th>
            <th>Société</th>
            <th>Email</th>
            <th>Num Tel</th>
          </tr>
        </thead> 
        <tbody className="Tab-body">
            <tr key={user.id}>
              <td>{user.username}</td>
              <td>{user.company}</td>
              <td>{user.email}</td>
              <td>{user.phoneNumber}</td>
            </tr>
        </tbody>
      </table>
    </div>
  );
}

export default Client;
