import MiniLogo from "../../assets/icons/miniLogo.png";
import "./loading.scss"
const LoadingScreen = () => {
  return (
    <div className='loading-screen'>
      <img alt='logo' src={MiniLogo} />
    </div>
  );
};

export default LoadingScreen;
