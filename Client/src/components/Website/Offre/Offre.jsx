import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'
import './offre.scss'
import { Link } from 'react-router-dom'
import React, { useState } from 'react';
import axios from 'axios';
//import "./Offre.css"
function CreateOffre() {

    const isAuthenticated = localStorage.getItem("user") !== null;
    const user = JSON.parse(localStorage.getItem("user"))?.user || null;


    const [formData, setFormData] = useState({
        ref: '',
        nom: '',
        Tonnage: '',
        caract: ''
    });
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);
        try {
            const response = await axios.post('/offre/add', formData); // Endpoint pour créer une nouvelle offre
            console.log('Offre créée avec succès:', response.data);
            setLoading(false);
            setError('');
            // Effacer le formulaire après avoir créé l'offre avec succès si nécessaire
            setFormData({
                ref: '',
                nom: '',
                Tonnage: '',
                caract: ''
            });
        } catch (error) {
            console.error('Erreur lors de la création de l\'offre:', error);
            setLoading(false);
            setError('Une erreur s\'est produite lors de la création de l\'offre.');
        }
    };

    return (
        <div>
            <div className="sticky top-0 left-0 right-0 z-40">
                <Navbar />
            </div>
            {isAuthenticated ? (
                <div>
                    <div className='offre-title'>Bienvenue Mr <span>{user.username}</span></div>
                    <table className='Table-titles'>
                        <tbody>
                            <tr><td className="table-label">Nom et Prenom :</td><td>{user.username}</td></tr>
                            <tr><td className="table-label">Email :</td><td>{user.email}</td></tr>
                            <tr><td className="table-label">Num° de Telephone :</td><td>{user.phoneNumber}</td></tr>
                            <tr><td className="table-label">Nom de Société :</td><td>{user.company}</td></tr>
                        </tbody>
                    </table>
                    <h2>Créer une nouvelle offre</h2>
                    {error && <p className="error-message">{error}</p>}
                    <form onSubmit={handleSubmit}>
                        <div>
                            <label htmlFor="ref" className="form-label">Référence:</label>
                            <input
                                type="text"
                                id="ref"
                                name="ref"
                                value={formData.ref}
                                onChange={handleChange}
                                required
                                className="form-input"
                            />
                        </div>
                        <div>
                            <label htmlFor="nom" className="form-label">Nom:</label>
                            <input
                                type="text"
                                id="nom"
                                name="nom"
                                value={formData.nom}
                                onChange={handleChange}
                                required
                                className="form-input"
                            />
                        </div>
                        <div>
                            <label htmlFor="Tonnage" className="form-label">Tonnage:</label>
                            <input
                                type="text"
                                id="Tonnage"
                                name="Tonnage"
                                value={formData.Tonnage}
                                onChange={handleChange}
                                required
                                className="form-input"
                            />
                        </div>
                        <div>
                            <label htmlFor="caract" className="form-label">Caractéristiques:</label>
                            <textarea
                                id="caract"
                                name="caract"
                                value={formData.caract}
                                onChange={handleChange}
                                required
                                className="form-textarea"
                            />
                        </div>
                        <button type="submit" disabled={loading} className="submit-button">
                            {loading ? 'En cours...' : 'Créer Offre'}
                        </button>
                    </form>
                </div>
            ) : (
                <div>
                    <div className="Offre-Not-Authed">
                        Pour Passer Une Demande d'Offre Veuillez Vous connecté
                    </div>
                    <div>
                        <Link to='/signin' className="signin-link">Connecté ici!</Link>
                    </div>
                </div>
            )}
            <Footer />
        </div >
    );
}

export default CreateOffre;