import React from 'react';
import LightButton from "../../../assets/website/light-mode-button.png";
import DarkButton from "../../../assets/website/dark-mode-button.png";

const DarkMode = () => {
  const [theme, setTheme] = React.useState(localStorage.getItem("theme") || "light");
  const element = document.documentElement; // Access to html elements

  // Set theme to localStorage and html element
  React.useEffect(() => {
    localStorage.setItem("theme", theme);
    if (theme === "dark") {
      element.classList.add("dark");
    } else {
      element.classList.remove("dark");
    }
  }, [theme]); // Add theme to dependency array

  return (
    <div className='relative'>
      <img
        onClick={() => setTheme(theme === "dark" ? "light" : "dark")}
        src={LightButton}
        alt="Light Mode"
        className={`w-12 cursor-pointer absolute right-0 z-10 ${theme === "dark" ? "opacity-0" : "opacity-100"}
        transition-all duration-300`}
        tabIndex="0" // Make image focusable
      />
      <img
        onClick={() => setTheme(theme === "dark" ? "light" : "dark")}
        src={DarkButton}
        alt="Dark Mode"
        className="w-12 cursor-pointer"
        tabIndex="0" // Make image focusable
      />
    </div>
  );
};

export default DarkMode;
