import { useRef } from "react";
import { Link } from "react-router-dom";
import { IoMdSearch } from "react-icons/io";
import { FaCaretDown } from "react-icons/fa6";
import { MdOutlineLocalOffer } from "react-icons/md";
Link
import logo from "../../../assets/hero/logo.png";
import DarkMode from "../Navbar/DarkMode";
import MA from "../../../assets/hero/12000_s.png";
import MAa from "../../../assets/hero/12000_s-1.png";
import JU from "../../../assets/hero/Zhaifr_VenusIII_6500.png";
import Image6 from "../../../assets/hero/robot.png";
import "./navbar.css";
import { useLogout } from "../../../hooks/useLogout";

const MenuLinks1 = [
  {
    id: 1,
    name: "Acceuil",
    link: "/#",
  },
];
const MenuLinks2 = [
  {
    id: 3,
    name: "Service",
    link: "/service",
  },
];



const Navbar = () => {
  const isAuthenticated = localStorage.getItem("user") !== null;
  const user = JSON.parse(localStorage.getItem("user"))?.user || null;

  const { logout } = useLogout();
  const form = useRef();

  const handleLogOut = () => {
    logout();
  };

  return (
    <div className="containerNav dark:bg-gray-900 dark:text-white navbar-wrapper">
      <div className="containerNavB">
        <div className="SizeContainer">
          {/* Logo and Link Section*/}
          <div className="LeftSide">
            <a href="#">
              <img src={logo} alt="" className="Logo" />
            </a>
            {/* Menu Items */}
            <div className="ListeSpace">
              <ul className="Liste">
                {MenuLinks1.map((data, index) => (
                  <li key={index}>
                    <a
                      href={data.link}
                      className="ListeItems dark:hover:text-white"
                    >
                      {data.name}
                    </a>
                  </li>
                ))}

                {/* DropDown section*/}
                <li className=" group ">
                  <a href="#" className="DropDownList dark:hover:text-white">
                    Produit
                    <span>
                      <FaCaretDown className="icon" />
                    </span>
                  </a>
                  <div className="DropDown dark:bg-gray-900 p-2 dark:text-white">
                    <div className="DropDownItems">
                      <div className="ProductDrop">
                        <div className="ProdRows">
                          <h1 className="ProdDown">Hydrolique</h1>
                          <ul className="ProdImg dark:text-white">
                            <li className="group">
                              <Link to='/UserArt'>
                                <img src={MA} alt="" />
                              </Link>

                            </li>
                          </ul>
                        </div>
                      </div>

                      <div className="ProductDrop">
                        <div className="ProdRows">
                          <h1 className="ProdDown">Electrique</h1>
                          <ul className="ProdImg dark:text-white">
                            <li className="group">
                              <Link to='/UserArt'>
                                <img src={JU} alt="" />
                              </Link>
                            </li>
                          </ul>
                        </div>
                      </div>

                      <div className="ProductDrop">
                        <div className="ProdRows">
                          <h1 className="ProdDown">Robots</h1>
                          <ul className="ProdImg dark:text-white">
                            <li className="group">
                              <Link to='/UserArt'>
                                <img src={Image6} alt="" />
                              </Link>
                            </li>
                          </ul>
                        </div>
                      </div>

                      <div className="ProductDrop">
                        <div className="ProdRows">
                          <h1 className="ProdDown">Peripherique</h1>
                          <ul className="ProdImg dark:text-white">
                            <li className="group">
                              <Link to='/UserArt'>
                                <img src={JU} alt="" />
                              </Link>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>

                {MenuLinks2.map((data, index) => (
                  <li key={index}>
                    <a
                      href={data.link}
                      className="ListeItems dark:hover:text-white"
                    >
                      {" "}
                      {data.name}
                    </a>
                  </li>
                ))}

                <li>
                  <div className="Offre dark:hover:text-white">
                    <Link to='/offre'>Offre</Link>
                  </div>
                </li>
                <li className="group ">
                  <a href="#" className="Contact dark:hover:text-white">
                    Contact
                    <span>
                      <FaCaretDown className="icon" />
                    </span>
                  </a>
                  {/* DropDown List */}
                  <div className=" ContactContent dark:bg-gray-900 dark:text-white">
                    <div className="DropDownItems">
                      <div className="ImgColumn" dir="ltr">
                        <img src={MAa} alt="" />
                        <div className="BlueSide"></div>
                        <div className="WhiteSide"></div>
                      </div>

                      <div>
                        <h1 className="ContactForm">Contact Us</h1>
                        <form action="" ref={form} className="form-contact">
                          <div className="ContactItems">
                            <label
                              className="ContactLabel dark:text-white"
                              htmlFor=""
                            >
                              Nom et Prenom
                            </label>
                            <input
                              name="user_name"
                              placeholder="Nom et Prenom"
                              className="ContactInput dark:bg-gray-900
                                                          dark:focus:border-white"
                              required
                              type="text"
                            />
                          </div>
                          <div className="ContactItems">
                            <label
                              className="ContactLabel dark:text-white peer-invalid:text-red-500
                                                         peer-valid:text-green-500"
                              htmlFor=""
                            >
                              E-mail
                            </label>
                            <input
                              name="user_email"
                              placeholder="ste.commercial.pmi@gmail.com"
                              className="ContactInput dark:bg-gray-900  dark:focus:border-white"
                              required
                              type="email"
                            />
                          </div>
                          <div className="ContactItems">
                            <label
                              className="ContactLabel dark:text-white"
                              htmlFor=""
                            >
                              Message
                            </label>
                            <textarea
                              name="message"
                              rows="4"
                              placeholder="Tappez votre Message ici..."
                              className="ContactInput dark:bg-gray-900  dark:focus:border-white"
                              required
                              type="text"
                            />
                          </div>
                          <div className="Contactbtn">
                            <button
                              type="submit"
                              value="Send"
                              className="Contbutton"
                            >
                              Envoyer
                            </button>
                          </div>
                        </form>
                      </div>

                      <div className="ContInfo">
                        <div className="Pos">
                          <h1>Contact Info</h1>
                          <div className="ContDet">
                            <p>Maher Felly</p>
                            <p>Rami Felly</p>
                            <p>Houssain Felly</p>
                            <p>+216 55 664 530</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>

          {/* Navbar Right Section*/}

          {/* Login / Logout Button Section */}
          <nav>
            {isAuthenticated ? (
              <div className="logged-in-user-nav-wrapper">
                <div className="user-nav-greeting">
                  Salut {user.username} !
                </div>
                <div className="my-offers">
                  <div className="offers-icon">
                    <MdOutlineLocalOffer />
                  </div>
                  <div className="offers-text">Mes Offres</div>
                </div>
                {/* <div className="my-commands">
                  <div className="command-icon">
                    <TbBasketUp />
                  </div>
                  <div className="command-text">Mes Commandes</div>{" "}
                </div> */}

                <div onClick={handleLogOut} className="navbar-signin-btn">
                  Sign Out
                </div>
              </div>
            ) : (
              <div className="RightSide">
                {/*Search Bar Section*/}
                <div className="relative group hidden sm:block">
                  <input
                    type="text"
                    placeholder="Search"
                    className="search-bar"
                  />
                  <IoMdSearch className="search-icon" />
                </div>

                {/*Dark Mode Section*/}
                <div>
                  <DarkMode />
                </div>

                <Link to="/signin" className="navbar-signin-btn">
                  Sign In
                </Link>
              </div>
            )}
          </nav>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
