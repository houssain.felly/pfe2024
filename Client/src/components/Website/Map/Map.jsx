import React from 'react'
import './Map.css'


const Map = () => {
    return (
        <div className='Cadre'>
            <div className='left'><h1>Localisation</h1></div>
            <div className='MapContent'>
                <div className='Map'>
                    <iframe width="100%" height="500" frameBorder="0" scrolling="no" marginHeight="0" marginWidth="0"
                        src="https://maps.google.com/maps?width=50%25&amp;height=300&amp;hl=en&amp;q=35.835027,%2010.593987+(PMI)&amp;t=&amp;z=15&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe>
                </div>
                <div className='Info'>
                    <div className='Empl'>
                        <h1>Contact Info</h1>
                        <div className='relative'>
                            <div>
                                <div class="contact-container">
                                    <ul class="contact-list">
                                        <li>Gérant</li>
                                        <li>Manager</li>
                                        <li>Secrétaire</li>
                                    </ul>
                                    <ul class="contact-list">
                                        <li>Maher Felly</li>
                                        <li>Rami Felly</li>
                                        <li>Sirine Hergli</li>
                                    </ul>
                                    <ul class="contact-list">
                                        <li>+216 20 555 567</li>
                                        <li>+216 23 366 132</li>
                                        <li>+216 55 664 530</li>
                                    </ul>
                                    <ul class="contact-list">
                                        <li>maher.felly@gmail.com</li>
                                        <li>ramy.felli@gmail.com</li>
                                        <li>Ste.commercial.pmi@gmail.com</li>
                                    </ul>
                                </div>

                                <strong><p>Ste.commercial.pmi@gmail.com</p></strong>
                                <p>Yasser Arafet, Sahloul, Sousse, Tunis</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Map
