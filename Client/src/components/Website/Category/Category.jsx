import React, { useEffect } from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';
import Hait from "../../../assets/hero/Haitian-PM.png";
import Jieken from "../../../assets/hero/logo-jieken.png";
import Shini from "../../../assets/hero/logo.svg";
import { LinkOutlined } from "@ant-design/icons";

import './Category.css';

const Category = () => {
  useEffect(() => {
    AOS.init({ duration: 1000 });
  }, []);

  return (
    <div>
      <h2 className='section-title'>Nos Partenaires</h2>
      <div className='category-container'>
        <div className='partner-section' data-aos="fade-up">
        <a href="https://haitianinter.com/en/" target="_blank" rel="noreferrer">
            <div>
              <LinkOutlined />
              <img className='Hait' src={Hait} alt="Haitian PM" />
            </div>
          </a>
          <p className='desc'>
            Il existe actuellement 60 centres de vente et de service régionaux pour les clients dans plus de 130 pays. En coopération avec les experts et les partenaires de service du groupe haitian, nous sommes non seulement en mesure de réagir rapidement aux nouvelles exigences du marché et du secteur, mais également de mettre en œuvre sur place des caractéristiques d'équipement individuelles, des certifications et de nouveaux niveaux d'application pour nos clients.
          </p>
        </div>

        <div className='partner-section' data-aos="fade-up" data-aos-delay="200">
          <a href="http://www.jiekenmould.com/" target="_blank" rel="noreferrer">
            <div>
              <LinkOutlined />
              <img className='Hait' src={Jieken} alt="Haitian PM" />
            </div>
          </a>
          <p className='desc'>
            Notre mission est de fournir des produits fiables et au rapport coût-performance le plus élevé, un excellent service et un excellent support comme base, et de servir ouvertement l'industrie mondiale du plastique. En même temps, cela offre des opportunités tant au personnel qu’aux partenaires.
          </p>
        </div>

        <div className='partner-section' data-aos="fade-up" data-aos-delay="400">
        <a href="https://www.shini.com/en/index.html" target="_blank" rel="noreferrer">
            <div>
              <LinkOutlined />
              <img className='Hait' src={Shini} alt="Haitian PM" />
            </div>
          </a>
          <p className='desc'>
            Notre mission est de fournir des produits fiables et au rapport coût-performance le plus élevé, un excellent service et un excellent support comme base, et de servir ouvertement l'industrie mondiale du plastique. En même temps, cela offre des opportunités tant au personnel qu’aux partenaires.
          </p>
        </div>
      </div>
    </div>
  );
}

export default Category;
