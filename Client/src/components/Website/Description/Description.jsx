import React, { useEffect } from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';
import logo from '../../../assets/hero/logo.png';
import './Description.css';

const Description = () => {
  useEffect(() => {
    AOS.init({ duration: 1000 }); // Personnalisez la durée si nécessaire
  }, []);

  return (
    <div className='container'>
      <div className='relativeClass'>
        <div className='FirstLigne' data-aos="fade-up">
          <img src={logo} alt="PMI Logo" />
        </div>
        <div className='SecondLigne' data-aos="fade-up" data-aos-delay="200">
          <p className='Desc'>
            <strong><strong>PMI</strong></strong> aujourd'hui Société Leader Grace aux efforts multiples entre Dirigeants et Partenaires International
            Leader de leurs tours dans le secteur de la plasturgie. <br />
            <strong>PMI</strong> a crée une plate forme technique maitrisant les techniques diverses de la plasturgie :<br />
            - Injection plastique<br />
            - Injection soufflage<br />
            - Extrusion de recyclage<br />
            - Extrusion et calandrage<br />
            - Conception et élaboration de moules<br />
            - Les systèmes IML<br />
            <strong>PMI</strong> s'est engagée à développer sa relation avec ses clients,
            Nous proposons le meilleur process et les techniques sur mesure les mieux adaptées pour un meilleur résultat.
            Vos besoins en Moules, en Machine et en Périphérique c'est notre souci quotidien.
            Avec une collaboration soutenue par des grande maison et de marque international PMl et sa filiale STDI font l'effort pour vous guider jusqu'à avoir la garantie du SAV. <br />
            <strong>PMI</strong> vous aidera à mieux canaliser et minimiser vos coûts de maintenances et entretiens. <br />
            <strong>PMI</strong> poursuit toujours un chemin professionnel partant des propositions de solutions à vos projets jusqu'à la mise en place et la concrétisation de ses solutions. <br />
            <strong>PMI</strong> S'ENGAGE EN L'INDUSTRIE.
          </p>
        </div>
      </div>
    </div>
  );
}

export default Description;
