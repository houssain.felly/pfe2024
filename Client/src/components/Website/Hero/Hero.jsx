import React from 'react';
import Slider from "react-slick";
import Image1 from "../../../assets/hero/12000_s.png";
import Image2 from "../../../assets/hero/JUIII24000_front.png";
import Image4 from "../../../assets/hero/Haitian-PM.png";
import Image5 from "../../../assets/hero/Zhafir.svg";
import Image6 from "../../../assets/hero/robot.png";
import Image7 from '../../../assets/hero/Zhaifr_VenusIII_6500.png'

import Button from '../Shared/Button';

import "./hero.css"

const HeroData = [
  {
    id: 1,
    img: Image1,
    subtitle: "Mars 5",
    title: Image4,
    title2: "LA TECHNOLOGIE AU POINT",
  },
  {
    id: 2,
    img: Image2,
    subtitle: "Jupiter",
    title: Image4,
    title2: "LA TECHNOLOGIE AU POINT",
  },
  {
    id: 1,
    img: Image7,
    subtitle: "Mars",
    title: Image5,
    title2: "LA TECHNOLOGIE AU POINT",
  },
  {
    id: 1,
    img: Image6,
    subtitle: "Mars",
    title: Image5,
    title2: "LA TECHNOLOGIE AU POINT",
  },
];

const Hero = () => {

  const settings = {
    dots: false,
    arrows: false,
    infinite: true,
    speed: 800,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    cssEase: "ease-in-out",
    pauseOnHover: false,
    pauseOnFocus: true,
  };

  return (
    <div className='containerSection'>
      <div className="heroWrapper">
        <div className="conta">
          {/* Hero Section */}
          <Slider {...settings}>
            {HeroData.map((data) => (
              <div key={data.id}>
                <div className="gridGridCols">
                  {/* text content section */}
                  <div className="colSpanFlex ">
                    {/*<h1 className="text-2xl sm:text-6xl lg:text-2xl font-bold">{data.subtitle}</h1>*/}
                    <img src={data.title} alt=""/>
                    <h1>{data.title2}</h1>
                    <div>
                      <Button
                        text="Details"
                        bgColor="bg-primary"
                        textcolor="text-white"
                      />
                    </div>
                  </div>
                  {/* Img section */}
                  <div className="colSpanTwo">
                    <div>
                      <img src={data.img} alt=""/>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </Slider>
        </div>
      </div>
    </div>
  )
}

export default Hero
