import React from 'react'
import NavbarAdmin from './AdminNavbar'
import AdminHero from '../AdminBasic/AdminBody'


const AdminBasicAll = () => {
  return (
    <div>
      <NavbarAdmin/>
      <AdminHero/>
    </div>
  )
}

export default AdminBasicAll