import React, { useRef } from 'react';


import { Link } from "react-router-dom"
import { IoMdSearch } from "react-icons/io"
import { FaCaretDown } from "react-icons/fa6"
import logo from '../../../assets/hero/logo.png'
import DarkMode from '../../Utilisateur/Navbar/DarkMode'
import MA from '../../../assets/hero/12000_s.png'
import MAa from '../../../assets/hero/12000_s-1.png'
import JU from '../../../assets/hero/Zhaifr_VenusIII_6500.png'
import Image6 from "../../../assets/hero/robot.png";
import "../../Utilisateur/Navbar/navbar.css"



const MenuLinks = [
    {
        id: 3,
        name: "Clients",
        link: "/user",
    },
    {
        id: 4,
        name: "Article",
        link: "/prod",
    },
    {
        id: 4,
        name: "Dashboard",
        link: "/dashboard",
    },
]



const NavbarAdmin = () => {
    return (
        <div>
            <div className='bg-white dark:bg-gray-900 select-none dark:text-white duration-200 relative z-40 navbar-wrapper' >
                <div className='py-4 relative'>
                    <div className="container flex justify-between items-center">
                        <div className='flex justify-end items-center'>
                            <div className='flex items-center gap-4'>
                                <div className='hidden lg:block'>
                                    <ul className='flex items-center gap-4'>
                                        {
                                            MenuLinks.map((data, index) => (
                                                <li key={index}>
                                                    <a href={data.link}
                                                        className='flex items-center px-4 font-semibold text-gray-500 
                                                   hover:text-black 
                                                   dark:hover:text-white duration-200'>
                                                        {" "}
                                                        {data.name}
                                                    </a>
                                                </li>
                                            ))}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        {/* Navbar Right Section*/}
                        <div className='flex justify-between items-center gap-4'>
                            {/* login-button Section */}
                            <div className='relative p-3 text-gray-500 hover:text-primary dark:hover:text-primary duration-200 font-semibold'>
                                <Link to='/'>Logout</Link>
                            </div>
                            {/*Dark Mode Section*/}
                            <div>
                                <DarkMode />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NavbarAdmin
