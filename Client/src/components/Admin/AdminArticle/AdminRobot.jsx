import { useState } from 'react';
import axios from 'axios';
import './AdminProduct.css';
import { FaArrowLeft } from 'react-icons/fa';
import { Link } from 'react-router-dom';





function AdminRobot() {
    // Define state variables
    const [productName, setProductName] = useState('');
    const [productPrice, setProductPrice] = useState('');
    const [FirstImage, setFirstImage] = useState(null);
    const [message, setMessage] = useState('');

    // Handle form submission
    const handleSubmit = async (e) => {
        e.preventDefault();

        // Create a new FormData object to handle file uploads
        const formData = new FormData();
        formData.append('name', productName);
        formData.append('price', productPrice);
        formData.append('image', FirstImage);

        try {
            // Send a POST request to the server to add the new product
            const response = await axios.post('http://localhost:3002/robot', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            });

            // Handle response and reset form
            if (response.status === 200) {
                setMessage('Produit ajouté avec succès !');
                setProductName('');
                setProductPrice('');
                setFirstImage(null);
            }
        } catch (error) {
            setMessage('Une erreur s\'est produite. Veuillez réessayer.');
            console.error(error);
        }
    };

    return (
        <div>
            <div className="top-left">
                <Link to="/admin">
                    <FaArrowLeft size={24} color="#007bff" /> {/* Arrow left icon */}
                </Link>
            </div>
            <div className="containerProd">
                <h1>Ajouter un nouveau Robot</h1>
                <form onSubmit={handleSubmit} className="form-containerProd">
                    {/* Product Name */}
                    <div className="form-group">
                        <label htmlFor="productName">Nom du Robot:</label>
                        <input
                            type="text"
                            id="productName"
                            className="form-control"
                            value={productName}
                            onChange={(e) => setProductName(e.target.value)}
                            required
                        />
                    </div>

                    {/* Product Price */}
                    <div className="form-group">
                        <label htmlFor="productPrice">Caracteristique du Robot:</label>
                        <input
                            type="text"
                            id="productPrice"
                            className="form-control"
                            value={productPrice}
                            onChange={(e) => setProductPrice(e.target.value)}
                            required
                        />
                    </div>

                    {/* Product Image */}
                    <div className="form-group">
                        <label htmlFor="productImage">Image du Robot:</label>
                        <input
                            type="file"
                            id="productImage"
                            className="form-control"
                            onChange={(e) => setFirstImage(e.target.files[0])}
                            required
                        />
                    </div>

                    {/* Submit Button */}
                    <button type="submit" className="btnProd btn-primary">Ajouter</button>

                    {/* Display Message */}
                    {message && <p>{message}</p>}
                </form>
            </div>
        </div>
    );
}

export default AdminRobot;
