import { Link } from 'react-router-dom';
import { FaPlus, FaEdit, FaTrash, FaEye } from 'react-icons/fa'; // Importing CRUD icons
import './AdminBodyAr.css';
import { FaArrowLeft } from 'react-icons/fa';


const AdminBodyAr = () => {
    return (
        <div>
            <Link to="/admin">
                <FaArrowLeft size={24} color="#007bff" /> {/* Arrow left icon */}
            </Link>
            <div className="admin-body-ar-container">
                {/* Column item for 'Machine' */}
                <div className="admin-body-ar-item">
                    <div className="crud-icons">
                        <FaPlus className="crud-icon" title="Create" />
                        <FaEdit className="crud-icon" title="Update" />
                        <FaTrash className="crud-icon" title="Delete" />
                        <FaEye className="crud-icon" title="Read" />
                    </div>
                    <Link to='./machine' className="admin-body-ar-link">Machine</Link>
                </div>

                {/* Column item for 'Robot' */}
                <div className="admin-body-ar-item">
                    <div className="crud-icons">
                        <FaPlus className="crud-icon" title="Create" />
                        <FaEdit className="crud-icon" title="Update" />
                        <FaTrash className="crud-icon" title="Delete" />
                        <FaEye className="crud-icon" title="Read" />
                    </div>
                    <Link to='./robot' className="admin-body-ar-link">Robot</Link>
                </div>

                {/* Column item for 'Peripherique' */}
                <div className="admin-body-ar-item">
                    <div className="crud-icons">
                        <FaPlus className="crud-icon" title="Create" />
                        <FaEdit className="crud-icon" title="Update" />
                        <FaTrash className="crud-icon" title="Delete" />
                        <FaEye className="crud-icon" title="Read" />
                    </div>
                    <Link to='./per' className="admin-body-ar-link">Peripherique</Link>
                </div>
            </div>
        </div>
    );
}

export default AdminBodyAr;
