import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import './Users.css';
import { FaArrowLeft } from 'react-icons/fa';

function Users() {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        const authToken = localStorage.getItem('authToken');


        axios.get('http://localhost:5000/user/AllUsers', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem({ authToken })}`
            }
        })
            .then(response => {
                console.log('User data:', response.data);
                setUsers(response.data);
            })
            .catch(error => {
                if (error.response) {
                    console.log('Error fetching user data:', error.response.status, error.response.statusText);
                } else {
                    console.log('Error fetching user data:', error.message);
                }
            });
    }, []);

    return (
        <div className="containers">
            <div className="top-left">
                <Link to="/admin">
                    <FaArrowLeft size={24} color="#007bff" aria-label="Go back" />
                </Link>
            </div>
            <div className="card">
                <table className="table">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Société</th>
                            <th>Email</th>
                            <th>Numéro de Téléphone</th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map((user) => (
                            <tr key={user._id}>
                                <td>{user.username}</td>
                                <td>{user.company}</td>
                                <td>{user.email}</td>
                                <td>{user.phoneNumber}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default Users;
